function net = vggFCN_fc678_mLoss_split0_init()
net = vggFCN_fc678_mLoss_split();
opts.sourceModelPath='../data/models/imagenet-vgg-verydeep-16.mat';
netVgg = vl_simplenn_tidy(load(opts.sourceModelPath)) ;
netVgg = dagnn.DagNN.fromSimpleNN(netVgg, 'canonicalNames', true) ;

Nb=2:2:30;Nk=1:2:29;
Nvb=2:2:30;Nvk=1:2:29;
for i=1:length(Nb)
    net.params(Nb(i)).value=netVgg.params(Nvb(i)).value;
    net.params(Nk(i)).value=netVgg.params(Nvk(i)).value;
    net.params(Nb(i)).learningRate = 2 ;
%     net.params(Nk(i)).learningRate = 0 ;
end
% seems very important, if has none will begin form 0.00915, but has form
% 0.03 instead
for j=[31 53 75]
    for i=[j:j+20]
        net.params(i).value=zeros(size(net.params(i).value),'single');
    end
    net.params(j+21).value=single(zeros(size(net.params(j+21).value)));
end