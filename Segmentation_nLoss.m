classdef Segmentation_nLoss < dagnn.Loss
    properties
        my_average=[0;0;0;0;0] ;% cumsum the epoch
        ep_average=[0;0;0;0;0] ;% calculate average every epoch
    end
    
    methods
        function outputs = forward(obj, inputs, params)
            mass = sum(sum(inputs{2} > 0,2),1) + 1 ;% add 0 to avoid divide 0
            outputs{1} = vl_nnloss(inputs{1}, inputs{2}, [], ...
                'loss', obj.loss, ...
                'instanceWeights', 1./mass) ;
            n = obj.numAveraged ;% last time number
            m = n + size(inputs{1},4) ;% this time batch size
            obj.ep_average(1) = (n * obj.ep_average(1) + double(gather(outputs{1}))) / m ;
            obj.numAveraged = m ;
            obj.average=obj.ep_average;
        end
        
        function [derInputs, derParams] = backward(obj, inputs, params, derOutputs)
            mass = sum(sum(inputs{2} > 0,2),1) + 1 ;
            derInputs{1} = vl_nnloss(inputs{1}, inputs{2}, derOutputs{1}, ...
                                        'loss', obj.loss, 'instanceWeights', 1./mass) ;
            derInputs{2} = [] ;
            derParams = {} ;
        end
        function reset(obj)
            obj.ep_average = [0;0;0;0;0] ;
            obj.numAveraged = 0 ;
        end
        function obj = Segmentation_nLoss(varargin)
            obj.load(varargin) ;
        end
    end
end