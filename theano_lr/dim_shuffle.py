Z#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Sat Nov 25 21:24:29 2017

@author: lz
"""
import theano as T
import numpy as np
import scipy.misc as misc
import os
os.chdir('/home/lz/theano_lr')
#gao ming!!!!!!!!!!!!!!!!!!!

a=T.tensor.ftensor3()
ones=T.shared(np.float32([[1,2,3],[7,8,9]]))

temp=ones.dimshuffle([0,1,'x'])
print(temp.eval())

temp=ones.dimshuffle([0,'x',1])
print(temp.eval())
print(temp.flatten(ndim=3).eval())
print(temp.flatten(ndim=2).eval())
print(temp.flatten(ndim=1).eval())


temp=ones.dimshuffle(['x',0,1])
print(temp.eval())

temp=ones.dimshuffle([1,0])
print(temp.eval())


l=np.load('l.npy')
#b=T.tensor.ftensor4(l)   #errrror
ones=T.shared(np.float32(l))
c1=ones.eval()
c2=ones.flatten(ndim=2).eval()
c3=ones.flatten(ndim=2).dimshuffle(1,0).eval()

print c1
misc.imshow(c1[0,:,:,1])

temp=ones.dimshuffle([0,3,1,2])
c2=np.array(temp.eval())
misc.imshow(c2[0,1,:,:])


#np.save('l.npy',l)