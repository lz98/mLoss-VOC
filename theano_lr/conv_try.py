#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Sun Nov 26 17:06:27 2017

@author: lz
"""
import theano 
import theano.tensor as T
import numpy as np
import scipy.misc as misc
import os
from theano.tensor.nnet.conv import conv2d  
from lasagne.layers import InputLayer, Conv2DLayer, MaxPool2DLayer, batch_norm, DropoutLayer, GaussianNoiseLayer
import lasagne

os.chdir('/home/lz/theano_lr')
l=np.load('l.npy')
#l=l[0,:,:,0]


a=theano.shared(l)
aimage=np.array(a.eval())
#aimage=np.array(a.dimshuffle([0,3,1,2]).eval())


net = {}
net['input'] = InputLayer(shape=(1,3,224,224), input_var=aimage)
net['conv_1'] = Conv2DLayer(net['input'], num_filters=1, filter_size=3, pad='valid')
net['conv_2'] = Conv2DLayer(net['conv_1'], num_filters=1, filter_size=3, pad='valid')
shape1=lasagne.layers.get_output_shape(net['conv_1'] )
aa1=lasagne.layers.get_output(net['conv_1'] ).eval()

shape2=lasagne.layers.get_output_shape(net['conv_2'] )
aa2=lasagne.layers.get_output(net['conv_2'] ).eval()