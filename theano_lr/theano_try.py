#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Sun Nov 26 16:50:19 2017

@author: lz
"""
from theano.tensor.nnet.conv import conv2d  
import theano
import numpy as np
import theano.tensor as T

inputs = T.tensor4(name='input', dtype='float64')

w_shp = (2, 1, 1, 2)
W = theano.shared(
    np.asarray(
        [
            [[[1., 1.]],
             [[1., 1.]]],
            [[[2., 2.]],
             [[1., 1.]]]
        ],
        dtype=inputs.dtype),
    name='W')

conv_out = conv2d(inputs, W)

f = theano.function([inputs], conv_out)

i = np.asarray([
    [[[1., 2., 3., 4., 5.]],
     [[1., 2., 3., 4., 5.]]]
], dtype='float64')
ii = f(i)
print(i.shape)
print(W.get_value().shape)
print(ii)
print(ii.shape)