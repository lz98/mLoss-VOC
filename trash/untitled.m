modelPath1 = 'mLossBN/net-epoch-20.mat';%navie

stats1=load(modelPath1,'stats') ;train1=stats1.stats.train;val1=stats1.stats.val;

ep=20;accind=6;
values=zeros(ep,1);
vvalues=zeros(ep,1);

for i=1:ep
    values(i,1)=gather(train1(i).accuracy(1));

    
    vvalues(i,1)=gather(val1(i).accuracy(1));
end
x=1:ep;
plot(x,values(:,1),'r-',...
    x,vvalues(:,1),'r-.'...
    );
legend('tscale1',...
    'vscale1')
grid on
saveas(gca,'mIou.png');



values=zeros(ep,3);
for i=1:ep
    values(i,1)=train1(i).objective(1);
    values(i,2)=train1(i).objective(2);
    values(i,3)=train1(i).objective(3);
end
x=1:ep;
plot(x,values(:,1),'r-',...
    x,values(:,2),'g-',...
    x,values(:,3),'b-')
legend('tscale1','tscale2','tscale3')
saveas(gca,'loss.png');