classdef Segmentation_mLoss < dagnn.Loss
    properties
        my_average=[0;0;0;0;0] ;% cumsum the epoch
        ep_average=[0;0;0;0;0] ;% calculate average every epoch
    end
    
    methods
        function outputs = forward(obj, inputs, params)
            mass = sum(sum(inputs{1} > 0,2),1) + 1 ;
            mainScale=1;%1+1
            outputs{mainScale} = vl_nnloss(inputs{mainScale+1}, inputs{1}, [], ...
                'loss', obj.loss, ...
                'instanceWeights', 1./mass) ;
            n = obj.numAveraged ;% last time number
            m = n + size(inputs{mainScale+1},4) ;% this time batch size
            obj.ep_average(mainScale) = (n * obj.ep_average(mainScale) + double(gather(outputs{mainScale}))) / m ;
            obj.numAveraged = m ;
            
            % here obj.loss equals 'softmaxlog'
            mconv={};msize={};mlabel={};mmass={};mot={};
            for i=setdiff([1,2,3],mainScale)
                mconv{i}=inputs{i+1};msize{i}=size(mconv{i});
                mlabel{i}=imresize(inputs{1}, msize{i}(1:2), 'nearest');
                mmass{i} = sum(sum(mlabel{i} > 0, 2),1) + 1 ;
                mot{i}=vl_nnloss(mconv{i}, mlabel{i}, [], 'loss', obj.loss, 'instanceWeights', 1./mmass{i});
                                
                obj.ep_average(i) = (n * obj.ep_average(i) + double(gather(mot{i}))) / m ;
            end
            obj.average=obj.ep_average;
        end
        
        function [derInputs, derParams] = backward(obj, inputs, params, derOutputs)
            mass = sum(sum(inputs{1} > 0,2),1) + 1 ;
            mainScale=1;
            derInputs{mainScale+1} = vl_nnloss(inputs{mainScale+1}, inputs{1}, 1, 'loss', obj.loss, 'instanceWeights', 1./mass) ;
            
            % here obj.loss equals 'softmaxlog'
            mconv={};msize={};mlabel={};mmass={};mder={};
            weight=[1,0,0];
            for i=setdiff([1,2,3],mainScale)
                mconv{i}=inputs{i+1};msize{i}=size(mconv{i});
                mlabel{i}=imresize(inputs{1}, msize{i}(1:2), 'nearest');
                mmass{i} = sum(sum(mlabel{i} > 0, 2),1) + 1 ;
                
                mder{i}=vl_nnloss(mconv{i}, mlabel{i}, 1, 'loss', obj.loss, 'instanceWeights', 1./mmass{i});
                derInputs{i+1}=weight(i)*mder{i};%1/2^((i+1)*2)*mder{i};%weight(i)*mder{i}
            end
            derInputs{1} = [] ;
            derParams = {} ;
        end
        function reset(obj)
            obj.ep_average = [0;0;0;0;0] ;
            obj.numAveraged = 0 ;
        end
        function obj = Segmentation_mLoss(varargin)
            obj.load(varargin) ;
        end
    end
end