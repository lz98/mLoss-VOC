modelPath1 = 'mLoss/net-epocha50-0.0001-MainScale-110.mat';%navie

stats1=load(modelPath1,'stats') ;train1=stats1.stats.train;val1=stats1.stats.val;

ep=50;accind=6;
values=zeros(ep,4);
vvalues=zeros(ep,4);

for i=1:ep
    values(i,1)=gather(train1(i).accuracy(1));
    values(i,2)=gather(train1(i).accuracy(2));
    values(i,3)=gather(train1(i).accuracy(3));
    values(i,4)=gather(train1(i).accuracy(4));
    
    vvalues(i,1)=gather(val1(i).accuracy(1));
    vvalues(i,2)=gather(val1(i).accuracy(2));
    vvalues(i,3)=gather(val1(i).accuracy(3));
    vvalues(i,4)=gather(val1(i).accuracy(4));
end
x=1:ep;
hold on;
plot(...
    x,values(:,4),'k-',...
    x,vvalues(:,4),'k-.'...
    );
legend('tscale-combine',...
    'vscale-combine')
T1=values(:,4);V1=vvalues(:,4);



modelPath1 = 'mLoss/net-epoch-50-0.001.mat';%navie
stats1=load(modelPath1,'stats') ;train1=stats1.stats.train;val1=stats1.stats.val;
ep=50;accind=6;
values=zeros(ep,2);
vvalues=zeros(ep,2);
for i=1:ep
    values(i,1)=gather(train1(i).accuracy(1));
    values(i,2)=gather(train1(i).accuracy(2));
    values(i,3)=gather(train1(i).accuracy(3));
%     values(i,4)=gather(train1(i).accuracy(4));
    
    vvalues(i,1)=gather(val1(i).accuracy(1));
    vvalues(i,2)=gather(val1(i).accuracy(2));
    vvalues(i,3)=gather(val1(i).accuracy(3));
%     vvalues(i,4)=gather(val1(i).accuracy(4));
end
x=1:ep;
hold on;
plot(...
    x,values(:,1),'g-',...
    x,vvalues(:,1),'g-.'...
    );
% grid on;
% legend('tscale-combine',...
%     'vscale-combine',...
%     'tnaive',...
%     'vnaive')
% saveas(gca,'mIou.png');
T2=values(:,1);V2=vvalues(:,1);


modelPath1 = 'mSplit/net-epocha50-0.0001-110-add210.mat';%navie
stats1=load(modelPath1,'stats') ;train1=stats1.stats.train;val1=stats1.stats.val;
ep=50;accind=6;
values=zeros(ep,4);
vvalues=zeros(ep,4);
for i=1:ep
    values(i,1)=gather(train1(i).accuracy(1));
    values(i,2)=gather(train1(i).accuracy(2));
    values(i,3)=gather(train1(i).accuracy(3));
    values(i,4)=gather(train1(i).accuracy(4));
    
    vvalues(i,1)=gather(val1(i).accuracy(1));
    vvalues(i,2)=gather(val1(i).accuracy(2));
    vvalues(i,3)=gather(val1(i).accuracy(3));
    vvalues(i,4)=gather(val1(i).accuracy(4));
end
x=1:ep;
hold on;
plot(...
    x,values(:,4),'b-',...
    x,vvalues(:,4),'b-.'...
    );
legend('tscale-combine-Split',...
    'vscale-combine-Split')
T3=values(:,4);V3=vvalues(:,4);



modelPath1 = 'meanSplit/net-epocha50-add110.mat';%navie
stats1=load(modelPath1,'stats') ;train1=stats1.stats.train;val1=stats1.stats.val;
ep=50;accind=6;
values=zeros(ep,4);
vvalues=zeros(ep,4);
for i=1:ep
    values(i,1)=gather(train1(i).accuracy(1));
    values(i,2)=gather(train1(i).accuracy(2));
    values(i,3)=gather(train1(i).accuracy(3));
    values(i,4)=gather(train1(i).accuracy(4));
    
    vvalues(i,1)=gather(val1(i).accuracy(1));
    vvalues(i,2)=gather(val1(i).accuracy(2));
    vvalues(i,3)=gather(val1(i).accuracy(3));
    vvalues(i,4)=gather(val1(i).accuracy(4));
end
x=1:ep;
hold on;
plot(...
    x,values(1:ep,1),'m-',...
    x,vvalues(1:ep,1),'m-.'...
    );
legend('tscale-combine-Mean',...
    'vscale-combine-Mean')
T4=values(1:ep,1);V4=vvalues(1:ep,1);

% legend('t-conv',...
%     'v-conv',...
%     't-naive',...
%     'v-naive',...
%     't-Split',...
%     'v-Split',...
%     't-Mean',...
%     'v-Mean'...
% )
% grid on
% saveas(gca,'compare4method.png')

% result combine
clf;
% plot(x,T1,'b',x,T2,'r',x,T3,'c',x,T4,'g')
% legend('1p1Conv','naive','split','average')

plot(x,T2,'r',x,T4,'g',x,T1,'b',x,T3,'c')
legend('naive','average','1p1Conv','split')
grid on;
saveas(gca,'compare4method-trn.png')

clf
plot(x,cumsum(V2)./[1:50]','r',x,cumsum(V4)./[1:50]','g',x,cumsum(V1)./[1:50]','b',x,cumsum(V3)./[1:50]','c')
legend('naive','average','1p1Conv','split')
grid on;
saveas(gca,'compare4method-val.png')
