function net = vggFCN_fc678_dropBN_naive_init()
net = vggFCN_fc678_dropBN_naive();
opts.sourceModelPath='../data/models/imagenet-vgg-verydeep-16.mat';
netVgg = vl_simplenn_tidy(load(opts.sourceModelPath)) ;
netVgg = dagnn.DagNN.fromSimpleNN(netVgg, 'canonicalNames', true) ;

Nb=2:2:32;Nk=1:2:31;
Nvb=2:2:32;Nvk=1:2:31;
for i=1:length(Nb)-1
    net.params(Nb(i)).value=netVgg.params(Nvb(i)).value;
    net.params(Nk(i)).value=netVgg.params(Nvk(i)).value;
    net.params(Nb(i)).learningRate = 2 ;
%     net.params(Nk(i)).learningRate = 0 ;
end
% seems very important, if has none will begin form 0.00915, but has form
% 0.03 instead and must be zeros!!! 4096*21
net.params(31).value=zeros(size(net.params(31).value),'single');
net.params(32).value=zeros(size(net.params(32).value),'single');
% even the last layer and if not have will lead to 0.03->0.02...
net.params(33).value=single(rand(size(net.params(33).value)));

