%% Loss
modelPath1 = 'mSplit/net-epocha50-0.0001-110-add210.mat';%navie
stats1=load(modelPath1,'stats') ;train1=stats1.stats.train;val1=stats1.stats.val;
ep=50;
values=zeros(ep,4);
vvalues=zeros(ep,4);
for i=1:ep
    values(i,1)=gather(train1(i).objective(1));
    values(i,2)=gather(train1(i).objective(2));
    values(i,3)=gather(train1(i).objective(3));
    values(i,4)=gather(train1(i).objective(4));
    
    vvalues(i,1)=gather(val1(i).objective(1));
    vvalues(i,2)=gather(val1(i).objective(2));
    vvalues(i,3)=gather(val1(i).objective(3));
    vvalues(i,4)=gather(val1(i).objective(4));
end
x=1:ep;
hold on;
plot(...
    x,values(:,1),'c-',...
    x,vvalues(:,1),'c-.'...
    );
legend('tscale-combine',...
    'vscale-combine')



modelPath1 = 'mLoss/net-epoch-50-0.001.mat';%navie
stats1=load(modelPath1,'stats') ;train1=stats1.stats.train;val1=stats1.stats.val;
ep=50;accind=6;
values=zeros(ep,2);
vvalues=zeros(ep,2);
for i=1:ep
    values(i,1)=gather(train1(i).objective(1));
    values(i,2)=gather(train1(i).objective(2));
    values(i,3)=gather(train1(i).objective(3));
    values(i,4)=gather(train1(i).objective(4));
    
    vvalues(i,1)=gather(val1(i).objective(1));
    vvalues(i,2)=gather(val1(i).objective(2));
    vvalues(i,3)=gather(val1(i).objective(3));
    vvalues(i,4)=gather(val1(i).objective(4));
end
x=1:ep;
hold on;
plot(...
    x,values(:,1),'r-',...
    x,vvalues(:,1),'r-.'...
    );
grid on;
legend('t-mLoss',...
    'v-mLoss',...
    't-nLoss',...
    'v-nLoss')
saveas(gca,'Loss_compare_multi-vs-naive-1.png');


%%  accuracy
clf;
modelPath1 = 'mSplit/net-epocha50-0.0001-110-add210.mat';%navie
stats1=load(modelPath1,'stats') ;train1=stats1.stats.train;val1=stats1.stats.val;
ep=50;
values=zeros(ep,4);
vvalues=zeros(ep,4);
for i=1:ep
    values(i,1)=gather(train1(i).accuracy(1));
    values(i,2)=gather(train1(i).accuracy(2));
    values(i,3)=gather(train1(i).accuracy(3));
    values(i,4)=gather(train1(i).accuracy(4));
    
    vvalues(i,1)=gather(val1(i).accuracy(1));
    vvalues(i,2)=gather(val1(i).accuracy(2));
    vvalues(i,3)=gather(val1(i).accuracy(3));
    vvalues(i,4)=gather(val1(i).accuracy(4));
end
x=1:ep;
hold on;
plot(...
    x,values(:,4),'c-',...
    x,vvalues(:,4),'c-.'...
    );
legend('tscale-combine',...
    'vscale-combine')


modelPath1 = 'mLoss/net-epoch-50-0.001.mat';%navie
stats1=load(modelPath1,'stats') ;train1=stats1.stats.train;val1=stats1.stats.val;
ep=50;accind=6;
values=zeros(ep,2);
vvalues=zeros(ep,2);
for i=1:ep
    values(i,1)=gather(train1(i).accuracy(1));
    values(i,2)=gather(train1(i).accuracy(2));
    values(i,3)=gather(train1(i).accuracy(3));
    values(i,4)=gather(train1(i).accuracy(4));
    
    vvalues(i,1)=gather(val1(i).accuracy(1));
    vvalues(i,2)=gather(val1(i).accuracy(2));
    vvalues(i,3)=gather(val1(i).accuracy(3));
    vvalues(i,4)=gather(val1(i).accuracy(4));
end
x=1:ep;
hold on;
plot(...
    x,values(:,1),'r-',...
    x,vvalues(:,1),'r-.'...
    );
grid on;
legend('t-mLoss',...
    'v-mLoss',...
    't-nLoss',...
    'v-nLoss')
saveas(gca,'mIou_compare_multi-vs-naive-1.png');