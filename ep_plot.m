modelPath1 = 'mLoss2/net-epocha25-mainScale1-100.mat';%navie

stats1=load(modelPath1,'stats') ;train1=stats1.stats.train;val1=stats1.stats.val;

ep=25;accind=6;
values=zeros(ep,4);
vvalues=zeros(ep,4);

for i=1:ep
    values(i,1)=gather(train1(i).accuracy(1));
    values(i,2)=gather(train1(i).accuracy(2));
    values(i,3)=gather(train1(i).accuracy(3));
    values(i,4)=gather(train1(i).accuracy(4));
    
    vvalues(i,1)=gather(val1(i).accuracy(1));
    vvalues(i,2)=gather(val1(i).accuracy(2));
    vvalues(i,3)=gather(val1(i).accuracy(3));
    vvalues(i,4)=gather(val1(i).accuracy(4));
end
x=1:ep;
plot(x,values(:,1),'r-',...
    x,values(:,2),'g-',...
    x,values(:,3),'b-',...
    x,values(:,4),'k-',...
    x,vvalues(:,1),'r-.',...
    x,vvalues(:,2),'g-.',...
    x,vvalues(:,3),'b-.',...
    x,vvalues(:,4),'k-.'...
    );
legend('tscale1','tscale2','tscale3','tscale-combine',...
    'vscale1','vscale2','vscale3','vscale-combine')
saveas(gca,'mIou.png');



values=zeros(ep,3);
for i=1:ep
    values(i,1)=train1(i).objective(1);
    values(i,2)=train1(i).objective(2);
    values(i,3)=train1(i).objective(3);
end
x=1:ep;
plot(x,values(:,1),'r-',...
    x,values(:,2),'g-',...
    x,values(:,3),'b-')
legend('tscale1','tscale2','tscale3')
saveas(gca,'loss.png');