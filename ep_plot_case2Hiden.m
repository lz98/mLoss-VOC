modelPath1 = 'mLoss/net-epocha50-0.0001-MainScale-110.mat';%navie

stats1=load(modelPath1,'stats') ;train1=stats1.stats.train;val1=stats1.stats.val;

ep=50;accind=6;
values=zeros(ep,4);
vvalues=zeros(ep,4);

for i=1:ep
    values(i,1)=gather(train1(i).accuracy(1));
    values(i,2)=gather(train1(i).accuracy(2));
    values(i,3)=gather(train1(i).accuracy(3));
    values(i,4)=gather(train1(i).accuracy(4));
    
    vvalues(i,1)=gather(val1(i).accuracy(1));
    vvalues(i,2)=gather(val1(i).accuracy(2));
    vvalues(i,3)=gather(val1(i).accuracy(3));
    vvalues(i,4)=gather(val1(i).accuracy(4));
end
x=1:ep;
hold on;
plot(...
    x,values(:,4),'k-',...
    x,vvalues(:,4),'k-.'...
    );
legend('tscale-combine',...
    'vscale-combine')

% x=1:ep;
% plot(x,values(:,1),'r-',...
%     x,values(:,2),'g-',...
%     x,values(:,4),'k-',...
%     x,vvalues(:,1),'r-.',...
%     x,vvalues(:,2),'g-.',...
%     x,vvalues(:,4),'k-.'...
%     );
% legend('tscale1','tscale2','tscale-combine',...
%     'vscale1','vscale2','vscale-combine')
% saveas(gca,'mIou.png');



modelPath1 = 'mLoss/net-epoch-50-0.001.mat';%navie
stats1=load(modelPath1,'stats') ;train1=stats1.stats.train;val1=stats1.stats.val;
ep=50;accind=6;
values=zeros(ep,2);
vvalues=zeros(ep,2);

for i=1:ep
    values(i,1)=gather(train1(i).accuracy(1));
    values(i,2)=gather(train1(i).accuracy(2));
    values(i,3)=gather(train1(i).accuracy(3));
%     values(i,4)=gather(train1(i).accuracy(4));
    
    vvalues(i,1)=gather(val1(i).accuracy(1));
    vvalues(i,2)=gather(val1(i).accuracy(2));
    vvalues(i,3)=gather(val1(i).accuracy(3));
%     vvalues(i,4)=gather(val1(i).accuracy(4));
end
x=1:ep;
hold on;
plot(...
    x,values(:,1),'g-',...
    x,vvalues(:,1),'g-.'...
    );
% grid on;
% legend('tscale-combine',...
%     'vscale-combine',...
%     'tnaive',...
%     'vnaive')
% saveas(gca,'mIou.png');


modelPath1 = 'mSplit/net-epocha50-0.0001-110-add210.mat';%navie
stats1=load(modelPath1,'stats') ;train1=stats1.stats.train;val1=stats1.stats.val;
ep=50;accind=6;
values=zeros(ep,4);
vvalues=zeros(ep,4);
for i=1:ep
    values(i,1)=gather(train1(i).accuracy(1));
    values(i,2)=gather(train1(i).accuracy(2));
    values(i,3)=gather(train1(i).accuracy(3));
    values(i,4)=gather(train1(i).accuracy(4));
    
    vvalues(i,1)=gather(val1(i).accuracy(1));
    vvalues(i,2)=gather(val1(i).accuracy(2));
    vvalues(i,3)=gather(val1(i).accuracy(3));
    vvalues(i,4)=gather(val1(i).accuracy(4));
end
x=1:ep;
hold on;
plot(...
    x,values(:,4),'b-',...
    x,vvalues(:,4),'b-.'...
    );
legend('tscale-combine-Split',...
    'vscale-combine-Split')



modelPath1 = 'meanSplit/net-epocha139-110-0.0001.mat';%navie
stats1=load(modelPath1,'stats') ;train1=stats1.stats.train;val1=stats1.stats.val;
ep=50;accind=6;
values=zeros(ep,4);
vvalues=zeros(ep,4);
for i=1:ep
    values(i,1)=gather(train1(i).accuracy(1));
    values(i,2)=gather(train1(i).accuracy(2));
    values(i,3)=gather(train1(i).accuracy(3));
    values(i,4)=gather(train1(i).accuracy(4));
    
    vvalues(i,1)=gather(val1(i).accuracy(1));
    vvalues(i,2)=gather(val1(i).accuracy(2));
    vvalues(i,3)=gather(val1(i).accuracy(3));
    vvalues(i,4)=gather(val1(i).accuracy(4));
end
x=1:ep;
hold on;
plot(...
    x,values(1:ep,1),'m-',...
    x,vvalues(1:ep,1),'m-.'...
    );
legend('tscale-combine-Mean',...
    'vscale-combine-Mean')



% modelPath1 = 'addHiden/net-epoch-50.mat';%navie
% stats1=load(modelPath1,'stats') ;train1=stats1.stats.train;val1=stats1.stats.val;
% ep=50;accind=6;
% values=zeros(ep,4);
% vvalues=zeros(ep,4);
% for i=1:ep
%     values(i,1)=gather(train1(i).accuracy(1));
%     values(i,2)=gather(train1(i).accuracy(2));
%     values(i,3)=gather(train1(i).accuracy(3));
%     values(i,4)=gather(train1(i).accuracy(4));
%     
%     vvalues(i,1)=gather(val1(i).accuracy(1));
%     vvalues(i,2)=gather(val1(i).accuracy(2));
%     vvalues(i,3)=gather(val1(i).accuracy(3));
%     vvalues(i,4)=gather(val1(i).accuracy(4));
% end
% x=1:ep;
% hold on;
% plot(...
%     x,values(:,4),'c-',...
%     x,vvalues(:,4),'c-.'...
%     );
% legend('tscale-combine-Hiden',...
%     'vscale-combine-Hiden')


legend('t-conv',...
    'v-conv',...
    't-naive',...
    'v-naive',...
    't-Split',...
    'v-Split',...
    't-Mean',...
    'v-Mean'...
)
grid on
saveas(gca,'compare4method.png')