function [net,ov_name] =addsplitLayer(net,inNum,outNum,iv_name,prefix)
numFmps=inNum;
N=outNum;    
for k=1:N; oV{k}=sprintf([prefix,'_split%d'],k); end

layername=[prefix,'_split'];
net.addLayer(layername, dagnn.Split('dim',3,'numClass',N), iv_name, oV,{});

iV=oV;classNum(1:N-1)=floor(numFmps/N);classNum(N)=numFmps-sum(classNum);
for k=1:N
    layername=sprintf([prefix,'_conv_1p1_%d'],k);oV{k}=layername;
    param=sprintf([prefix,'_conv_1p1_%d_params'],k);
    convBlock = dagnn.Conv('size', [1,1,classNum(k),1], 'hasBias', false, 'pad',[0,0,0,0]);
    net.addLayer(layername, convBlock, iV{k}, oV{k}, param);
end

iV=oV;
layername=[prefix,'afterSplit'];ov_name=layername;
net.addLayer(layername, dagnn.Concat('dim',3), iV, ov_name,{});
