modelPath1 = 'mLoss/net-epocha50-0.0001-MainScale-110.mat';%navie

stats1=load(modelPath1,'stats') ;train1=stats1.stats.train;val1=stats1.stats.val;

ep=50;accind=6;
values=zeros(ep,4);
vvalues=zeros(ep,4);

for i=1:ep
    vvalues(i,1)=gather(val1(i).accuracy(1));
    vvalues(i,2)=gather(val1(i).accuracy(2));
    vvalues(i,3)=gather(val1(i).accuracy(3));
    vvalues(i,4)=gather(val1(i).accuracy(4));
    vvalues(i,5)=gather(val1(i).accuracy(5));
    vvalues(i,6)=gather(val1(i).accuracy(6));
end
T=[];
T=[T;max(vvalues(:,4)),max(vvalues(:,5)),max(vvalues(:,6))]




modelPath1 = 'mLoss/net-epoch-50-0.001.mat';%navie
stats1=load(modelPath1,'stats') ;train1=stats1.stats.train;val1=stats1.stats.val;
ep=50;accind=6;
values=zeros(ep,2);
vvalues=zeros(ep,2);
for i=1:ep
    vvalues(i,1)=gather(val1(i).accuracy(1));
    vvalues(i,2)=gather(val1(i).accuracy(2));
    vvalues(i,3)=gather(val1(i).accuracy(3));
    vvalues(i,4)=gather(val1(i).accuracy(4));
    vvalues(i,5)=gather(val1(i).accuracy(5));
    vvalues(i,6)=gather(val1(i).accuracy(6));
end
T=[T;max(vvalues(:,4)),max(vvalues(:,5)),max(vvalues(:,6))]



modelPath1 = 'mSplit/net-epocha50-0.0001-110-add210.mat';%navie
stats1=load(modelPath1,'stats') ;train1=stats1.stats.train;val1=stats1.stats.val;
ep=50;accind=6;
values=zeros(ep,4);
vvalues=zeros(ep,4);
for i=1:ep
    vvalues(i,1)=gather(val1(i).accuracy(1));
    vvalues(i,2)=gather(val1(i).accuracy(2));
    vvalues(i,3)=gather(val1(i).accuracy(3));
    vvalues(i,4)=gather(val1(i).accuracy(4));
    vvalues(i,5)=gather(val1(i).accuracy(5));
    vvalues(i,6)=gather(val1(i).accuracy(6));
end
T=[T;max(vvalues(:,4)),max(vvalues(:,5)),max(vvalues(:,6))]




modelPath1 = 'meanSplit/net-epocha50-add110.mat';%navie
stats1=load(modelPath1,'stats') ;train1=stats1.stats.train;val1=stats1.stats.val;
ep=50;accind=6;
values=zeros(ep,4);
vvalues=zeros(ep,4);
for i=1:ep
    vvalues(i,1)=gather(val1(i).accuracy(1));
    vvalues(i,2)=gather(val1(i).accuracy(2));
    vvalues(i,3)=gather(val1(i).accuracy(3));
    vvalues(i,4)=gather(val1(i).accuracy(4));
    vvalues(i,5)=gather(val1(i).accuracy(5));
    vvalues(i,6)=gather(val1(i).accuracy(6));
end
T=[T;max(vvalues(:,4)),max(vvalues(:,5)),max(vvalues(:,6))]
